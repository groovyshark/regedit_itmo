﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Win32;
using System.Security.AccessControl;

namespace MyRegedit
{
	public partial class Form1 : Form
	{

		private string oldRegName;

		public Form1()
		{
			InitializeComponent();
		}

		private void Form1_Load(object sender, EventArgs e)
		{
			RegistryKey rk = Registry.ClassesRoot;
			treeView1.Nodes.Add(rk.Name);
			treeView1.Nodes[0].Nodes.Add(string.Empty);
			treeView1.Nodes[0].Tag = rk;

			rk = Registry.CurrentUser;
			treeView1.Nodes.Add(rk.Name);
			treeView1.Nodes[1].Nodes.Add(string.Empty);
			treeView1.Nodes[1].Tag = rk;

			rk = Registry.LocalMachine;
			treeView1.Nodes.Add(rk.Name);
			treeView1.Nodes[2].Nodes.Add(string.Empty);
			treeView1.Nodes[2].Tag = rk;

			rk = Registry.Users;
			treeView1.Nodes.Add(rk.Name);
			treeView1.Nodes[3].Nodes.Add(string.Empty);
			treeView1.Nodes[3].Tag = rk;

			rk = Registry.CurrentConfig;
			treeView1.Nodes.Add(rk.Name);
			treeView1.Nodes[4].Nodes.Add(string.Empty);
			treeView1.Nodes[4].Tag = rk;

			treeView1.BeforeExpand += new TreeViewCancelEventHandler(treeView1_BeforeExpand);

		}

		void treeView1_BeforeExpand(object sender, TreeViewCancelEventArgs e)
		{
			if (e.Node.Nodes.Count == 1 && e.Node.Nodes[0].Text == string.Empty)
			{
				e.Node.Nodes.Clear();
				RegistryKey key = e.Node.Tag as RegistryKey;
				if (key != null)
				{
					foreach (string name in key.GetSubKeyNames())
					{
						e.Node.Nodes.Add(name, name);
						if (name != "SECURITY" && name != "SAM")
						{
							RegistryKey subkey = key.OpenSubKey(name, 
								RegistryKeyPermissionCheck.ReadWriteSubTree, 
								RegistryRights.FullControl);

							e.Node.Nodes[name].Tag = subkey;
							if (subkey.SubKeyCount > 0)
								e.Node.Nodes[name].Nodes.Add(string.Empty);
						}
					}
				}
			}
		}

		private void treeView1_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
		{

		}

		private void treeView1_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
		{
			listView1.Items.Clear();
			RegistryKey rk = (RegistryKey)treeView1.SelectedNode.Tag;

			foreach(var value in rk.GetValueNames())
			{
				string name = value;
				string type = rk.GetValueKind(value).ToString();
				string val = rk.GetValue(name).ToString();
				if (rk.GetValueKind(value) == RegistryValueKind.DWord )
				{
					int a = (Int32)rk.GetValue(name);
					val += " (0x" + a.ToString("X8") + ")";
				}
				else if (rk.GetValueKind(value) == RegistryValueKind.QWord)
				{
					long a = (Int64)rk.GetValue(name);
					val += " (0x" + a.ToString("X8") + ")";
				}

				ListViewItem item1 = new ListViewItem(new string[] { name, type, val } );

				if (!listView1.Items.Contains(item1))
				{
					listView1.Items.Add(item1);
				}

			}

		}


		private void treeView1_MouseClick(object sender, MouseEventArgs e)
		{

		}

		private void editToolStripMenuItem_Click(object sender, EventArgs e)
		{
			if (listView1.SelectedItems.Count == 0)
				return;

			RegistryKey rk = (RegistryKey)treeView1.SelectedNode.Tag;
			var item = listView1.SelectedItems[0];
			string name = item.SubItems[0].Text;

			EditForm ef = new EditForm(name, rk.GetValueKind(name), rk.GetValue(name), treeView1.SelectedNode.Tag);

			ef.ShowInTaskbar = false;
			ef.Owner = this;
			ef.Show();
		}

		private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
		{
			if (listView1.SelectedItems.Count == 0)
				return;

			DialogResult dialogResult = MessageBox.Show("Are you really want to delete this parameter?", 
												"Confirm delete parameter", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
			if (dialogResult == DialogResult.OK)
			{
				RegistryKey rk = (RegistryKey)treeView1.SelectedNode.Tag;
				var item = listView1.SelectedItems[0];
				string name = item.SubItems[0].Text;

				rk.DeleteValue(name);
			}
			else if (dialogResult == DialogResult.Cancel)
				return;
		}

		private void renameToolStripMenuItem_Click(object sender, EventArgs e)
		{
			if (listView1.SelectedItems.Count == 0)
				return;

			var name = listView1.SelectedItems[0].SubItems[0].Text;
			RenameForm rf = new RenameForm(name, treeView1.SelectedNode.Tag);

			rf.ShowInTaskbar = false;
			rf.Owner = this;
			rf.Show();
		}

		private void listView1_AfterLabelEdit(object sender, LabelEditEventArgs e)
		{

		}

		private void listView1_BeforeLabelEdit(object sender, LabelEditEventArgs e)
		{
			oldRegName = listView1.SelectedItems[0].SubItems[0].Text;
		}

		private void createToolStripMenuItem_Click(object sender, EventArgs e)
		{
			CreateForm cf = new CreateForm(treeView1.SelectedNode.Tag);

			cf.ShowInTaskbar = false;
			cf.Owner = this;
			cf.Show();
		}

		private void listView1_SelectedIndexChanged(object sender, EventArgs e)
		{

		}
	}
}

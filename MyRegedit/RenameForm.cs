﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Win32;

namespace MyRegedit
{
	public partial class RenameForm : Form
	{
		string name_;
		RegistryKey rk;

		public RenameForm(string name, object tag)
		{
			InitializeComponent();

			name_ = name;
			rk = (RegistryKey)tag;

			textBox1.Text = name_;
		}

		private void RenameForm_Load(object sender, EventArgs e)
		{

		}

		private void button2_Click(object sender, EventArgs e)
		{
			Close();
		}

		private void button1_Click(object sender, EventArgs e)
		{
			string name = textBox1.Text;

			rk.SetValue(name, rk.GetValue(name_), rk.GetValueKind(name_));
			rk.DeleteValue(name_);

			Close();
		}
	}
}

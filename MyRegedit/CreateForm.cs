﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Win32;

namespace MyRegedit
{
	public partial class CreateForm : Form
	{
		RegistryKey rk;

		public CreateForm(object tag)
		{
			InitializeComponent();

			rk = (RegistryKey)tag;
		}

		private void label1_Click(object sender, EventArgs e)
		{

		}

		private void button1_Click(object sender, EventArgs e)
		{
			string name = textBox1.Text;
			string value = textBox2.Text;

			RegistryValueKind type;
			switch (comboBox1.Text)
			{
				case "String":
					type = RegistryValueKind.String;
					rk.SetValue(name, value, type);
					break;
				case "DWord":
					type = RegistryValueKind.DWord;
					rk.SetValue(name, Convert.ToInt32(value), type);
					break;
				case "QWord":
					type = RegistryValueKind.QWord;
					rk.SetValue(name, Convert.ToInt64(value), type);
					break;
				case "ExpandString":
					type = RegistryValueKind.ExpandString;
					rk.SetValue(name, value, type);
					break;
				case "MultiString":
					type = RegistryValueKind.MultiString;
					rk.SetValue(name, value, type);
					break;
			}
			Close();
		}

		private void button2_Click(object sender, EventArgs e)
		{
			Close();
		}
	}
}

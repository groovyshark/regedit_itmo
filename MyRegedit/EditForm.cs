﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Win32;
using System.Security.AccessControl;

namespace MyRegedit
{
	public partial class EditForm : Form
	{
		private string name_;
		private RegistryValueKind type_;
		private object value_;
		RegistryKey rk_;
		private int fromBase = 10;

		public EditForm(string name, RegistryValueKind type, object value, object tag)
		{
			InitializeComponent();

			name_ = name;
			type_ = type;
			value_ = value;
			rk_ = (RegistryKey)tag;

			textBox1.Text = name;
			textBox2.Text = value_.ToString();

			if (!(type_ == RegistryValueKind.QWord || 
				type_ == RegistryValueKind.DWord ||
				type_ == RegistryValueKind.Binary))
				groupBox1.Visible = false;

		}

		private void label1_Click(object sender, EventArgs e)
		{

		}

		private void groupBox1_Enter(object sender, EventArgs e)
		{

		}


		private void radioButton2_CheckedChanged(object sender, EventArgs e)
		{
			string value = textBox2.Text;
			if (radioButton2.Checked)
			{
				textBox2.Text = Convert.ToInt32(value, 10).ToString("X8");
				fromBase = 16;
			}
			else if (radioButton1.Checked)
			{
				textBox2.Text = Convert.ToInt32(value, 16).ToString();
				fromBase = 10;
			}

		}

		private void button2_Click(object sender, EventArgs e)
		{
			Close();
		}

		private void button1_Click(object sender, EventArgs e)
		{
			string value = textBox2.Text;
			switch (type_)
			{
				case RegistryValueKind.String:
					rk_.SetValue(name_, value, type_);
					break;
				case RegistryValueKind.DWord:
					rk_.SetValue(name_, Convert.ToInt32(value, fromBase), type_);
					break;
				case RegistryValueKind.QWord:
					rk_.SetValue(name_, Convert.ToInt64(value, fromBase), type_);
					break;
				case RegistryValueKind.ExpandString:
					rk_.SetValue(name_, value, type_);
					break;
				case RegistryValueKind.MultiString:
					rk_.SetValue(name_, value, type_);
					break;
			}
			
			Close();
		}
	}
}
